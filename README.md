# go-proxy-sdk

#### 介绍
这是一个关于如何使用青果网络下代理IP产品的sdk包，本仓库为golang版本。目前主要包括了动态共享按时，动态共享按量，以及动态独享等三款产品的使用代码示例。具体如何使用sdk请见_example目录下的main.go文件。

#### 包中的文件说明

##### pool文件夹
ippool是一个IP池的类，主要实现了对IP资源的管理，以及提供了获取IP的函数。支持定时刷新或者手动刷新IP池中的资源，提供手动标记IP不可用的情况

##### crawler文件夹
crawler是一个爬虫的实现类，主要是使用ippool中的IP资源进行爬虫请求，使用者可以自定义爬虫的处理函数Task，以及代理协议、代理验证方式等。
