package crawler

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"sync/atomic"
	"time"

	"golang.org/x/net/context"
	"golang.org/x/time/rate"

	"git.qg.net/backend/proxyip/pool"
)

type AuthWay string

const (
	AuthByBasic    AuthWay = "basic"
	AuthByClientIP AuthWay = "clientip"
)

type Protocol string

const (
	Socks5 Protocol = "socks5"
	Http   Protocol = "http"
)

type BasicAuth struct {
	Username string
	Password string
}

func (a BasicAuth) String() string {
	return fmt.Sprintf("%s:%s", a.Username, a.Password)
}

type TaskFn func(proxyIP *pool.ProxyIP, httpCli *http.Client) error

var DefaultCrawler = &Crawler{
	AuthWay:       AuthByClientIP,
	ProxyProtocol: Http,
	RateLimiter:   rate.NewLimiter(1, 1),
	Task: func(proxyIP *pool.ProxyIP, httpCli *http.Client) error {
		req, _ := http.NewRequest(http.MethodGet, "https://ip.cn/api/index?ip=&type=0", nil)
		resp, err := httpCli.Do(req)
		if err != nil {
			return fmt.Errorf("request failed, proxy ip: %s %s, err: %s\n", proxyIP.ServerAddr, proxyIP.IP, err)
		}
		defer resp.Body.Close()

		body, _ := io.ReadAll(resp.Body)
		fmt.Printf("time: %s, response body: %s\n", time.Now().Format("2006-01-02 15:04:05"), string(body))
		return nil
	},
	shutdown: false,
}

type Option func(*Crawler)

func WithIPPool(ipPool *pool.IPPool) Option {
	return func(c *Crawler) {
		c.IPPool = ipPool
	}
}

func WithProtocol(protocol Protocol) Option {
	return func(c *Crawler) {
		c.ProxyProtocol = protocol
	}
}

func WithByBasicAuth(basicAuth BasicAuth) Option {
	return func(c *Crawler) {
		c.AuthWay = AuthByBasic
		c.BasicAuth = basicAuth
	}
}

func WithRateLimit(rateLimit int) Option {
	return func(c *Crawler) {
		c.RateLimiter = rate.NewLimiter(rate.Limit(rateLimit), rateLimit)
	}
}

func WithTaskFn(taskFn TaskFn) Option {
	return func(c *Crawler) {
		c.Task = taskFn
	}
}

type Crawler struct {
	IPPool        *pool.IPPool
	AuthWay       AuthWay
	ProxyProtocol Protocol
	BasicAuth     BasicAuth
	RateLimiter   *rate.Limiter
	Task          TaskFn

	shutdown bool
	running  int64
}

func New(opts ...Option) *Crawler {
	crawler := DefaultCrawler
	for _, option := range opts {
		option(crawler)
	}
	return crawler
}

func (c *Crawler) Start() {
	for {
		if c.shutdown {
			break
		}

		// 限流
		c.RateLimiter.Wait(context.Background())

		proxyIP := c.IPPool.Next()
		// ip池暂时没有IP
		if proxyIP == nil {
			continue
		}

		var proxyUrl *url.URL
		if c.AuthWay == AuthByBasic {
			proxyUrl, _ = url.Parse(fmt.Sprintf("%s://%s@%s", c.ProxyProtocol, c.BasicAuth, proxyIP.ServerAddr))
		} else {
			proxyUrl, _ = url.Parse(fmt.Sprintf("%s://%s", c.ProxyProtocol, proxyIP.ServerAddr))
		}

		atomic.AddInt64(&c.running, 1)
		go func(ip *pool.ProxyIP, url *url.URL) {
			defer atomic.AddInt64(&c.running, -1)

			err := c.Task(ip, &http.Client{
				Transport: &http.Transport{
					Proxy: http.ProxyURL(url),
				},
				Timeout: time.Second * 5,
			})
			if err != nil {
				ip.MarkUnusable()
			}
		}(proxyIP, proxyUrl)
	}
}

func (c *Crawler) Stop(ctx context.Context) error {
	c.shutdown = true

	ch := make(chan struct{})
	go func() {
		timer := time.NewTimer(time.Second * 3)
		ticker := time.NewTicker(time.Millisecond * 100)
		for {
			select {
			case <-timer.C:
				ch <- struct{}{}
			case <-ticker.C:
				if atomic.LoadInt64(&c.running) == 0 {
					ch <- struct{}{}
				}
			}
		}
	}()

	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-ch:
		return nil
	}
}
