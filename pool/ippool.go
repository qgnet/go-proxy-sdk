package pool

import (
	"encoding/json"
	"fmt"
	"io"
	"net"
	"net/http"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

type ProxyIP struct {
	IP         string
	ServerAddr string
	Deadline   time.Time
	Usable     bool
}

func (ip *ProxyIP) MarkUnusable() {
	ip.Usable = false
}

const (
	dePoolCollectAddr  = "https://proxy.qg.net/replace"
	dstPoolCollectAddr = "https://proxy.qg.net/extract"
	dsnPoolCollectAddr = "https://proxy.qg.net/allocate"
)

var httpCli = &http.Client{
	Transport: &http.Transport{
		DialContext: (&net.Dialer{
			Timeout: 2 * time.Second,
		}).DialContext,
		MaxIdleConnsPerHost: 10,
		MaxIdleConns:        10,
		IdleConnTimeout:     10 * time.Minute,
	},
	Timeout: 15 * time.Second,
}

type IPResult struct {
	IP       string `json:"IP"`
	Port     string `json:"port"`
	Deadline string `json:"deadline"`
	Host     string `json:"host"`
}

type ResponseBody struct {
	Code int         `json:"Code"`
	Msg  string      `json:"Msg"`
	Data []*IPResult `json:"Data"`
}

/*
 * 动态独享IP池，使用replace接口作为提取接口，支持用户定时更换或者手动更换
 */

type IPPool struct {
	url    string
	ips    []*ProxyIP
	cursor int

	lock   sync.Mutex
	logger *logrus.Logger
}

// NewDeIPPool
/**
 * 动态独享资源池，使用/replace接口作为提取接口
 */
func NewDeIPPool(key, pwd string, capacity int, logger *logrus.Logger) (*IPPool, error) {
	pool := &IPPool{
		url:    fmt.Sprintf("%s?Key=%s&Pwd=%s&Num=%d", dePoolCollectAddr, key, pwd, capacity),
		lock:   sync.Mutex{},
		logger: logger,
	}
	err := pool.Collector()
	return pool, err
}

// NewDstIPPool
/**
 * 动态共享按时资源池，使用/extract接口作为提取接口
 */
func NewDstIPPool(key, pwd string, capacity int, logger *logrus.Logger) (*IPPool, error) {
	pool := &IPPool{
		url:    fmt.Sprintf("%s?Key=%s&Pwd=%s&Num=%d", dstPoolCollectAddr, key, pwd, capacity),
		lock:   sync.Mutex{},
		logger: logger,
	}
	err := pool.Collector()
	return pool, err
}

// NewDsnIPPool
/**
 * 动态共享按量资源池，使用/allocate接口作为提取接口
 */
func NewDsnIPPool(key, pwd string, capacity int, logger *logrus.Logger) (*IPPool, error) {
	pool := &IPPool{
		url:    fmt.Sprintf("%s?Key=%s&Pwd=%s&Num=%d", dsnPoolCollectAddr, key, pwd, capacity),
		lock:   sync.Mutex{},
		logger: logger,
	}
	err := pool.Collector()
	return pool, err
}

func (p *IPPool) DeleteIP(ip string) {
	for i, proxyIP := range p.ips {
		if proxyIP.IP == ip {
			p.ips = append(p.ips[:i], p.ips[i+1:]...)
		}
	}
}

func (p *IPPool) Collector() error {
	req, _ := http.NewRequest(http.MethodGet, p.url, nil)
	resp, err := httpCli.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	result := new(ResponseBody)
	err = json.Unmarshal(body, result)
	if err != nil {
		return fmt.Errorf("failed to unmarshal response body, body: %s", string(body))
	}

	ips := make([]*ProxyIP, len(result.Data))
	for i, datum := range result.Data {
		deadline, _ := time.Parse("2006-01-02 15:04:05", datum.Deadline)
		ips[i] = &ProxyIP{
			IP:         datum.IP,
			ServerAddr: datum.Host,
			Deadline:   deadline,
			Usable:     true,
		}
	}

	p.lock.Lock()
	defer p.lock.Unlock()
	p.ips = ips
	p.cursor = 0
	return nil
}

func (p *IPPool) AutoRefreshPool(interval time.Duration) {
	ticker := time.NewTicker(interval)
	defer ticker.Stop()

	for {
		<-ticker.C
		if err := p.Collector(); err != nil {
			p.logger.WithError(err).Errorln("failed to auto refresh pool")
		}
	}
}

func (p *IPPool) GetAllIPs() []*ProxyIP {
	return p.ips
}

func (p *IPPool) Next() *ProxyIP {
	p.lock.Lock()
	defer p.lock.Unlock()

	var ip *ProxyIP
	var count int
	for {
		ip = p.ips[p.cursor]
		p.cursor = (p.cursor + 1) % len(p.ips)
		// 检验IP的截至时间是否有效，并且未标记为不可用
		if ip.Deadline.Sub(time.Now()).Seconds() > 3 && ip.Usable {
			break
		}
		// 避免没有可用IP时出现死循环
		if count > len(p.ips) {
			return nil
		}
		count++
	}

	return ip
}

func (p *IPPool) Range(f func(ip *ProxyIP)) {
	p.lock.Lock()
	defer p.lock.Unlock()

	for _, ip := range p.ips {
		f(ip)
	}
}
