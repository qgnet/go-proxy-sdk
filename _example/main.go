package main

import (
	"time"

	"github.com/sirupsen/logrus"

	"git.qg.net/backend/proxyip/crawler"
	"git.qg.net/backend/proxyip/pool"
)

func main() {
	// 动态独享40s刷新一次IP池，请求频率1请求/s
	dynamicExclusive("DBDAAAA2", "5BC6169482A6")
	// 动态共享按时62s刷新一次IP池，请求频率1请求/s
	dynamicShareByTime("3A658781", "89B9266DE571")
	// 动态共享按量20s刷新一次IP池，请求频率1请求/s
	dynamicShareByNum("MDZ3H2G0", "029K7YF2DE69")
}

func dynamicExclusive(key, password string) {
	ipPool, err := pool.NewDeIPPool(key, password, 5, logrus.New())
	if err != nil {
		panic(err)
	}
	go ipPool.AutoRefreshPool(time.Second * 40)

	c := crawler.New(crawler.WithIPPool(ipPool), crawler.WithByBasicAuth(crawler.BasicAuth{
		Username: key,
		Password: password,
	}), crawler.WithRateLimit(1))

	go c.Start()
}

func dynamicShareByTime(key, password string) {
	ipPool, err := pool.NewDstIPPool(key, password, 5, logrus.New())
	if err != nil {
		panic(err)
	}
	go ipPool.AutoRefreshPool(time.Second * 62)

	c := crawler.New(crawler.WithIPPool(ipPool), crawler.WithByBasicAuth(crawler.BasicAuth{
		Username: key,
		Password: password,
	}), crawler.WithRateLimit(1))

	go c.Start()
}

func dynamicShareByNum(key, password string) {
	ipPool, err := pool.NewDsnIPPool(key, password, 5, logrus.New())
	if err != nil {
		panic(err)
	}
	go ipPool.AutoRefreshPool(time.Second * 20)

	c := crawler.New(crawler.WithIPPool(ipPool), crawler.WithByBasicAuth(crawler.BasicAuth{
		Username: key,
		Password: password,
	}), crawler.WithRateLimit(1))

	go c.Start()
}
